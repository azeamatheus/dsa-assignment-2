import ballerina/io;
import ballerina/uuid;
import ballerinax/kafka;

kafka:ProducerConfiguration producerConfigs = {
    clientId: "bixa-plaza-producer",
    acks: "all",
    retryCount: 3,
    //only one copy of each message
    enableIdempotence: true,
    transactionalId: "test-transactional-id",
    //The files have a limited size of 30 MB
    sendBuffer: 30
};

// remote server endpoints of Kafka brokers
final string DEFAULT_URL = "localhost:9092";

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL, producerConfigs);

public function main() {
    json StorageMessage = {
        message: "Kafka provides durability and it can be used to feed events to complex event streaming systems"
    };

    byte[] serializedMessage = StorageMessage.toBalString().toBytes();

    kafkaAdvancedTransactionalProduce(serializedMessage);

}

function kafkaAdvancedTransactionalProduce(byte[] message) {
    transaction {
        kafka:Error? sendResult = kafkaProducer->send({
            topic: "bixa-plaza",
            value: message,
            'key: uuid:createType1AsString().toBytes()
        });

        //Checks for an error and notifies if an error has occurred.
        if sendResult is kafka:Error {
            io:println("Error occurred while sending message ", sendResult);
        }

        var commitResult = commit;
        if commitResult is () {
            io:println("Transaction successful");
        } else {
            io:println("Transaction unsuccessful " + commitResult.message());
        }
    }
}